import pickle
from sklearn.utils import shuffle
from utils.util import *
from utils.data import load_dataset
from models.stgcnneo import Stgcnneo
from train.train import train_step
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

""" GPU execution setup """
config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.45
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

max_neighborhood_size = 0

global_data = np.load(GLOBAL_DATA_FILE_NAME).astype(np.float32)
# load of data segments

# graph loading
rag_file = open(RAG_FILE_NAME, 'rb')
rag = pickle.load(rag_file)
rag_file.close()
nodes = rag.nodes()
for id in nodes:
    max_neighborhood_size = max(len(list(rag.neighbors(id))), max_neighborhood_size)


x_train, x_validation, x_test, y_train, y_validation, y_test, id_train, id_validation, id_test = load_dataset()
# load of the dataset already splitted into train, validation and test set

train_neighborhood_sizes = get_neighborhood_sizes(id_train, rag)
valid_neighborhood_sizes = get_neighborhood_sizes(id_validation, rag)
test_neighborhood_sizes = get_neighborhood_sizes(id_test, rag)
# load of the neighborhood effective sizes

train_neighborhoods, mask_train = get_neighborhoods_and_mask(id_train, rag, global_data, max_neighborhood_size)
valid_neighborhoods, mask_valid = get_neighborhoods_and_mask(id_validation, rag, global_data, max_neighborhood_size)
test_neighborhoods, mask_test = get_neighborhoods_and_mask(id_test, rag, global_data, max_neighborhood_size)

# neighborhoods padding and masking

model = Stgcnneo(output_units=512, dropout_rate=0.4, n_classes=N_CLASSES)

""" Model Optimization & Save setup """
loss_object = tf.keras.losses.CategoricalCrossentropy()
optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001)
ckpt = tf.train.Checkpoint(step=tf.Variable(1), optimizer=optimizer, model=model)
manager = tf.train.CheckpointManager(ckpt, OUTPUT_FOLDER, max_to_keep=1)

""" TRAINING """
iterations = x_train.shape[0] / BATCH_SIZE
if x_train.shape[0] % BATCH_SIZE != 0:
    iterations += 1

best_step = -1
best_val_accuracy = 0.0
for e in range(EPOCHS):
    x_train, y_train, train_neighborhoods, train_neighborhood_sizes, mask_train = shuffle(x_train, y_train,
                                              train_neighborhoods, train_neighborhood_sizes, mask_train, random_state=0)
    train_neighborhoods = shuffle_non_zero(train_neighborhoods, train_neighborhood_sizes)
    """ neighborhoods data sampling """
    t_train_neighborhoods = train_neighborhoods[:, 0:SAMPLING_SIZE, :, :]
    t_valid_neighborhoods = valid_neighborhoods[:, 0:SAMPLING_SIZE, :, :]
    t_test_neighborhoods = test_neighborhoods[:, 0:SAMPLING_SIZE, :, :]

    t_mask_train = mask_train[:, 0:SAMPLING_SIZE]
    t_mask_valid = mask_valid[:, 0:SAMPLING_SIZE]
    t_mask_test = mask_test[:, 0:SAMPLING_SIZE]
    start = time.time()
    train_accuracy, train_loss, validation_accuracy = train_step(model, x_train, y_train, optimizer, loss_object,
                                 x_validation=x_validation, y_validation=y_validation, train_neighborhoods=t_train_neighborhoods,
                                 valid_neighborhoods=t_valid_neighborhoods, mask_train=t_mask_train, mask_valid=t_mask_valid,
                                 train_dims=np.expand_dims(np.array(train_neighborhood_sizes).astype("float32"), axis=-1),
                                 valid_dims=np.expand_dims(np.array(valid_neighborhood_sizes).astype("float32"), axis=-1), iterations=iterations, batch_size=BATCH_SIZE)
    best_val_accuracy = print_epoch_metrics(train_accuracy, validation_accuracy, train_loss, best_val_accuracy, e, manager, start, ckpt)

# TESTING
test_accuracy, test_fscore, class_fscore, test_kappa_score = test_step(model, manager, ckpt, x_test, y_test,
                                                                       test_neighborhoods, mask_test,
                                                                       test_neighborhood_sizes)
print_results("STGCNNeo final results: ", test_accuracy, test_fscore, class_fscore, test_kappa_score)
