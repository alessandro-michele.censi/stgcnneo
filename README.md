# STGCNNeo
Neighborhood abstraction:

<img src="img/Neighborhood.png" alt="Neighborhood" width="750"/>
<p>
Spatio-Temporal Graph Convolutional Neural Network for Earth Observation:</p>
<p>
<!-- model architecture image here -->
![alt text](img/STGCNNeo_Architecture.png "STGCNNeo Architecture")
</p>
<p>Details on CNN Architecture adopted:</p>
<p>
![alt text](img/cnn.png "CNN Architecture")
</p>

# Overview
In this project we provide the implementation of a Spatio Temporal Graph Convolutional Neural Network for Earth Observation data in TensorFlow 2.0, along with an execution example (on the Dordogne earth observation dataset). The repository is organised as follows:
 - `models/`: constains the model classes implementation;
 - `train/`: contains the script used in the model training process;
 - `utils/`: contains various utility scripts such as constants or functions like printing and data loading;
 - `main.py`: is the executable script of the project. It loads all the necessary data, train the model through them and test it;
 
# Dependencies
The entire project has been tested running under Python 3.7.4, with the following packages installed (along with their dependencies):
  - `tensorflow==2.1.0`;
  - `tensorflow-gpu==2.1.0`;
  - `numpy==1.18.1`;
  - `scikit-learn==0.22.1`;

In addition, CUDA 10.2 has been used.

# Dataset setup
In order to execute the project correctly, the dataset must be splitted as follows:
  - Split data into train, validation and test set naming each file;
  - Do the same for the target labels of the dataset naming each file;
  - Starting from the labelled segments, get all the segment ids relating to them and do the the same of the previous points;
  - Now put all the data in a folde and copy/paste the path in the `BASE_DIR_FOLD` constant of the file `constants.py`;
  - Update the constant `BASE_FILE_NAMES` in `constants.py` with the correspondant file names of the data;
  - Put in a folder also the RAG (Region Adjancency Graph) relative to the dataset and the segments data and update the `constants.py` paths of them;

The data are then reshaped in the main file in order to get a shape of (n_segment_objects, n_timestamps, n_bands).

# Execution guide
Simple run `python main.py` statement in your terminal after going after going in the project's direcory.

# Main Features
Spatio-Temporal Graph Convolutional Neural Networks (STGCNNs) aim to cope, simultaneously, with both spatial and temporal dependencies exhibited by SITS data. Leveraging the framework of Spatio-Temporal Graph Convolutional Neural Networks, we define the STGCNNeo model as follows:
```math
 \boxed{h_i = f_1(TS(v_i)) \space\space \Big\Vert \space\space \frac 1 {\lvert N(v_i) \rvert} \cdot \space\space\space\space \sum_{\mathclap{v_j \in N(v_i)}} \space\space\space\space \normalsize f_2(TS(v_j))}
```
where $`N(v_i)`$ is the set of neighborhood of the segment $`v_i`$ , $`v_j`$ is a generic neighbor of $`v_i`$ , $`f_1`$ is the neural network that process the information associated to the segment $`v_i`$ f while $`f_2`$ is the neural network that process the neighborhood information and is the concatenation operator. We underline that $`f_2`$ does not depend on a particular $`v_j`$ but the same network (the same set of learnable weights) process each segment in $`N(v_i)`$.

In our case, due to the fact that $`TS(v_i)`$ return time series information, we adopt a neural network that explicitly copes with temporal data. To this end, we conceive $`f_1(·)`$ and $`f_2(·)`$ as One dimension Convolutional Neural Networks (CNN). For both neural networks we adopt the same CNN architecture which is composed by the following layers:


In addition, to better perform 1D convolutions on neighborhoods data through timestamps and cope with the different neighborhood sizes, we padded and masked them (in order to have all of the neighborood sizes equal to the maximum size in the graph), and then we applied the CNN model using a Recurrent Cell on each neighbor. This process could be graphically described as follow:
```mermaid
graph BT
    subgraph RNN
        subgraph RNN_Cell
        h0((Cnn 1D))
        end
        neighbor_1 --> h0  --> o1([output_1])
        subgraph RNN Cell
        h1((Cnn 1D))
        end
        neighbor_2 --> h1 --> o2([output_2])
        subgraph RNN_Cell.
        ht((...))
        end
        id3[...] --> ht --> ot([...])
        subgraph RNN Cell_
        hn-1((Cnn 1D))
        end
        neighbor_n-1 --> hn-1 --> on-1([output_n-1])
        subgraph RNN Cell
        hn((Cnn 1D))
        end
        neighbor_n --> hn --> on([output_n])
    end
    
    style RNN fill:#ffffb1;
```

To summarize, STGCNNeo firstly considers the temporal dimension by means of a CNN module that perform convolution on the temporal dimension and, only later in the process, it leverages the spatial information carried out by the spatial (neighborhood) context of the target node $`v_i`$ .
<!--#Reference 

```
@article{
  velickovic2018graph,
  title="{Graph Attention Networks}",
  author={Veli{\v{c}}kovi{\'{c}}, Petar and Cucurull, Guillem and Casanova, Arantxa and Romero, Adriana and Li{\`{o}}, Pietro and Bengio, Yoshua},
  journal={International Conference on Learning Representations},
  year={2018},
  url={https://openreview.net/forum?id=rJXMpikCZ},
  note={accepted as poster},
}
```
-->

# Licence

