import numpy as np
from utils.util import BASE_DIR_FOLDS, BASE_FILE_NAMES, EXTENSION
from utils.util import reshape_to_tensor

"""
    :return the dataset already splitted in train/validation/test sets;
    :return data labels
    :return data segmentation ids
"""


def load_dataset():

    # make filenames using the current data fold number
    train_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[0] + EXTENSION
    validation_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[1] + EXTENSION
    test_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[2] + EXTENSION

    target_train_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[3] + EXTENSION
    target_validation_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[4] + EXTENSION
    target_test_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[5] + EXTENSION

    id_train_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[6] + EXTENSION
    id_validation_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[7] + EXTENSION
    id_test_fn = BASE_DIR_FOLDS + "/" + BASE_FILE_NAMES[8] + EXTENSION

    # load of data
    x_train = np.load(train_fn).astype(np.float32)
    x_validation = np.load(validation_fn).astype(np.float32)
    x_test = np.load(test_fn).astype(np.float32)

    # reshaping of data into tensors of shape: (n_examples, n_timestamps, n_feature_bands)
    x_train = reshape_to_tensor(x_train)
    x_validation = reshape_to_tensor(x_validation)
    x_test = reshape_to_tensor(x_test)

    # load of labels
    y_train = np.load(target_train_fn)
    y_validation = np.load(target_validation_fn)
    y_test = np.load(target_test_fn)

    # load of segment ids
    id_train = np.load(id_train_fn)
    id_validation = np.load(id_validation_fn)
    id_test = np.load(id_test_fn)

    return x_train, x_validation, x_test, y_train, y_validation, y_test, id_train, id_validation, id_test
