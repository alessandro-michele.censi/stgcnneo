BASE_DIR_FOLDS = '../../Dordogne-Dataset/fold 1'
EXTENSION = '.npy'
BASE_FILE_NAMES = ['x_train_1', 'x_validation_1', 'x_test_1', 'y_train_1', 'y_validation_1',
                   'y_test_1', 'id_train_1', 'id_validation_1', 'id_test_1']
OBJECTS_GT_FILE_NAME = '../../Dordogne-Dataset/slic_object_id_dordogne.npy'
GLOBAL_DATA_FILE_NAME = '../../Dordogne-Dataset/slic_data_dordogne.npy'
RAG_FILE_NAME = '../../Dordogne-Dataset/rag_segmentation_dordogne'
OUTPUT_FOLDER = './STGCNN_model/stgcnneo_dordogne'

N_TIMESTAMPS = 23
N_BANDS = 6
EPOCHS = 3000
N_CLASSES = 7
BATCH_SIZE = 32
SAMPLING_SIZE = 10
