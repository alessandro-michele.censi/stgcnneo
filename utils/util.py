import numpy as np
import pandas as pd
import tensorflow as tf
import sys
import time
from sklearn.metrics import accuracy_score, f1_score, cohen_kappa_score
from utils.constants import *


"""
    :return numpy array of data reshaped in (n_examples, n_timestamps, n_feature_bands)
"""


def reshape_to_tensor(x):
    new_data = []
    for row in x:
        new_row = np.split(row, N_TIMESTAMPS)
        new_data.append(np.array(new_row))
    return np.array(new_data)


"""
    :return a new batch of data
"""


def get_batch(x, i, batch_size):
    start_id = i * batch_size
    end_id = min((i + 1) * batch_size, x.shape[0])
    batch_x = x[start_id:end_id]
    return batch_x


""" compute accuracy, f-score and kappa score for the training and validation set. The model is saved if validation 
accuracy is better than the previous best. """


def print_epoch_metrics(train_accuracy, validation_accuracy, train_loss, best_epoch_val_accuracy, epoch, manager, start, ckpt):
    print("epoch %d loss %f Train Accuracy %f" % (epoch, train_loss, np.round(train_accuracy, 4)))
    if validation_accuracy >= best_epoch_val_accuracy:
        # new best model found, so save the checkpoint into a file
        best_epoch_val_accuracy = validation_accuracy
        best_step = int(ckpt.step)
        save_path = manager.save()
        print("Saved checkpoint for step {}: {}".format(best_step, save_path))
        print("Accuracy {:1.4f}".format(best_epoch_val_accuracy))
    print("vs. Validation Accuracy %f" % validation_accuracy)
    end = time.time()
    print("epoch time: %f" % (end - start))
    print("===============")
    sys.stdout.flush()
    return best_epoch_val_accuracy


"""
    prints on std output the average values of performance metrics (accuracy, f-score, kappa score) obtained from each 
    data fold.
"""


def print_results(model_name, accuracy, f_score, f_score_per_class, k_score):
    print(model_name + " results:")
    print("Accuracy (avg): ", accuracy)
    print("F score (avg): ", f_score)
    print("K score (avg): ", k_score)
    f = np.array(f_score_per_class)
    for i in range(N_CLASSES):
        print("F-score (avg) class {0} : {1}".format(int(i + 1), f[i]))


"""
    :return a randomly shuffled data
"""


def shuffle_non_zero(x, dims):
    for i in range(len(x)):
        idx = np.arange(dims[i])
        perm = np.random.permutation(idx)
        x[i][idx] = x[i][perm]
    return x


"""
    :return numpy array of the effective neighborhood sizes for each node of the graph
"""


def get_neighborhood_sizes(ids, rag):
    dims = []
    for id in ids:
        dims.append(len(list(rag.neighbors(id))))
    return np.array(dims)



"""
    :return a padded version of the neighborhood data and a mask according an upper bound size of the neighborhood
"""


def get_neighborhoods_and_mask(ids, rag, data, max_neighborhood_size=0):
    neighborhoods = []
    for id in ids:
        nx = [n for n in rag.neighbors(id)]
        data_neighbors = data[nx]
        neighborhoods.append(data_neighbors)
    padded_neighborhoods = tf.keras.preprocessing.sequence.pad_sequences(neighborhoods,
                                                                         maxlen=max_neighborhood_size,
                                                                         padding='post', dtype='float32')
    reshaped_padded_neighborhoods = []
    for p in range(len(padded_neighborhoods)):
        reshaped_padded_neighborhoods.append(reshape_to_tensor(padded_neighborhoods[p]))
    reshaped_padded_neighborhoods = np.array(reshaped_padded_neighborhoods)

    mask = np.zeros((reshaped_padded_neighborhoods.shape[0], max_neighborhood_size), dtype=np.bool)
    for i in range(len(neighborhoods)):
        mask[i, 0:len(neighborhoods[i])] = True

    return reshaped_padded_neighborhoods, mask


"""
    Makes predictions on test set and prints results   
"""


def test_step(model, manager, ckpt, x_test, y_test, test_neighborhoods, test_mask, test_dims):
    ckpt.restore(manager.latest_checkpoint)
    t_test_neighborhoods = shuffle_non_zero(test_neighborhoods, test_dims)
    t_test_neighborhoods = t_test_neighborhoods[:, 0:SAMPLING_SIZE, :, :]
    t_test_mask = test_mask[:, 0:SAMPLING_SIZE]

    test_predictions = model.predict_by_batch([x_test, t_test_neighborhoods, t_test_mask,
                                                np.expand_dims(np.array(test_dims).astype("float32"), axis=-1)],
                                                batch_size=BATCH_SIZE)

    accuracy = accuracy_score(np.argmax(y_test, axis=1), np.argmax(test_predictions, axis=1))
    fscore = f1_score(np.argmax(y_test, axis=1), np.argmax(test_predictions, axis=1), average='weighted')
    kappa_score = cohen_kappa_score(np.argmax(y_test, axis=1), np.argmax(test_predictions, axis=1))
    per_class_fscore = f1_score(np.argmax(y_test, axis=1), np.argmax(test_predictions, axis=1), average=None)
    return accuracy, fscore, per_class_fscore, kappa_score
