import tensorflow as tf

"""
    Convolutional Neural Network Model
"""


class Cnn(tf.keras.Model):
    def __init__(self, filters=512, kernel_size=3, dropout_rate=0.4, activation='relu', name='CNN1D', **kwargs):
        super(Cnn, self).__init__(name=name, **kwargs)

        self.conv1 = tf.keras.layers.Conv1D(filters=int(filters / 2), kernel_size=kernel_size, activation=activation, name="conv_layer_1")
        self.bn1 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_1")
        self.dropout1 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_1")
        self.conv2 = tf.keras.layers.Conv1D(filters=int(filters / 2), kernel_size=kernel_size, activation=activation, name="conv_layer_2")
        self.bn2 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_2")
        self.dropout2 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_2")
        self.conv3 = tf.keras.layers.Conv1D(filters=int(filters / 2), kernel_size=kernel_size, activation=activation, name="conv_layer_3")
        self.bn3 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_3")
        self.dropout3 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_3")
        self.conv4 = tf.keras.layers.Conv1D(filters=int(filters / 2), kernel_size=kernel_size, activation=activation, name="conv_layer_4")
        self.bn4 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_4")
        self.dropout4 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_4")

        self.conv5 = tf.keras.layers.Conv1D(filters=filters, strides=2, kernel_size=3, activation=activation, name="conv_layer_5")
        self.bn5 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_5")
        self.dropout5 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_5")
        self.conv6 = tf.keras.layers.Conv1D(filters=filters, kernel_size=3, activation=activation, name="conv_layer_6")
        self.bn6 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_6")
        self.dropout6 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_6")
        self.conv7 = tf.keras.layers.Conv1D(filters=filters, kernel_size=1, activation=activation, name="conv_layer_7")
        self.bn7 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_7")
        self.dropout7 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_7")
        self.conv8 = tf.keras.layers.Conv1D(filters=filters, kernel_size=1, activation=activation, name="conv_layer_8")
        self.bn8 = tf.keras.layers.BatchNormalization(name="batch_normalization_layer_8")
        self.dropout8 = tf.keras.layers.Dropout(rate=dropout_rate, name="dropout_layer_8")
        self.pool = tf.keras.layers.GlobalAveragePooling1D()

    def call(self, inputs, training=False):
        conv1 = self.conv1(inputs)
        conv1 = self.bn1(conv1, training=training)
        conv1 = self.dropout1(conv1, training=training)
        conv2 = self.conv2(conv1)
        conv2 = self.bn2(conv2, training=training)
        conv2 = self.dropout2(conv2, training=training)
        conv3 = self.conv3(conv2)
        conv3 = self.bn3(conv3, training=training)
        conv3 = self.dropout3(conv3, training=training)
        conv4 = self.conv4(conv3)
        conv4 = self.bn4(conv4, training=training)
        conv4 = self.dropout4(conv4, training=training)
        conv5 = self.conv5(conv4)
        conv5 = self.bn5(conv5, training=training)
        conv5 = self.dropout5(conv5, training=training)
        conv6 = self.conv6(conv5)
        conv6 = self.bn6(conv6, training=training)
        conv6 = self.dropout6(conv6, training=training)
        conv7 = self.conv7(conv6)
        conv7 = self.bn7(conv7, training=training)
        conv7 = self.dropout7(conv7, training=training)
        conv8 = self.conv8(conv7)
        conv8 = self.bn8(conv8, training=training)
        conv8 = self.dropout8(conv8, training=training)
        concat = tf.concat((conv8, conv7), axis=2)
        return self.pool(concat)
