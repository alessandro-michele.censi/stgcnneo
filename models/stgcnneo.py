import tensorflow as tf
from utils.util import get_batch
from layers.recurrent_cell import RecurrentCell
from models.cnn1d import Cnn

"""
    SpatioTemporal Graph Convolutional Neural Network Model
"""


class Stgcnneo(tf.keras.Model):
    def __init__(self, output_units, n_classes, dropout_rate=0.0, hidden_activation='relu', output_activation='softmax',
                 name='STGCNNeo', **kwargs):
        super(Stgcnneo, self).__init__(name=name, **kwargs)
        self.output_units = output_units
        self.f1 = Cnn(filters=self.output_units, kernel_size=3, dropout_rate=dropout_rate, name="temporalCnn")
        self.f2 = tf.keras.layers.RNN(RecurrentCell(self.output_units, dropout=dropout_rate), return_sequences=True, name="neighborhoodsRNN")
        self.fc1 = tf.keras.layers.Dense(units=output_units, activation=hidden_activation, name="fully_connected_1")
        self.bn1 = tf.keras.layers.BatchNormalization(name="batchnormalization_1")
        self.fc2 = tf.keras.layers.Dense(units=output_units, activation=hidden_activation, name="fully_connected_2")
        self.bn2 = tf.keras.layers.BatchNormalization(name="batchnormalization_2")
        self.output_layer = tf.keras.layers.Dense(units=n_classes, activation=output_activation, name="output_layer")

    @tf.function
    def call(self, inputs, training=False):
        target_nodes = inputs[0]  # target nodes data shape: (BATCH_SIZE, N_TIMESTAMPS, N_BANDS)
        neighborhood_nodes = inputs[1]  # neighborhoods data shape: (BATCH_SIZE, N_NEIGHBORHOODS, N_TIMESTAMPS, N_BANDS)
        mask = inputs[2]  # mask shape (BATCH_SIZE, N_NEIGHBORHOODS)
        neighborhood_sizes = inputs[3]  # shape: (BATCH_SIZE, 1)

        temporal_output = self.f1(target_nodes, training=training)  # output shape: (BATCH_SIZE, 1024)
        temporal_neighborhood_output = self.f2(neighborhood_nodes, mask=mask, training=training)
        # temporal_neighborhood output shape: (BATCH_SIZE, N_NEIGHBORHOODS, 2*OUTPUT_UNITS)

        temporal_neighborhood_output = tf.reduce_sum(temporal_neighborhood_output, axis=1)
        # output shape: (BATCH_SIZE, 2*OUTPUT_UNITS)
        temporal_neighborhood_output = tf.math.divide_no_nan(temporal_neighborhood_output, neighborhood_sizes)

        hidden_state = tf.concat([temporal_output, temporal_neighborhood_output], 1)
        # hidden_state shape: (BATCH_SIZE, 4*OUTPUT_UNITS)

        output = self.fc1(hidden_state)  # output shape: (BATCH_SIZE, OUTPUT_UNITS)
        output = self.bn1(output, training=training)
        output = self.fc2(output)
        output = self.bn2(output, training=training)
        return self.output_layer(output)  # result shape: (BATCH_SIZE, N_CLASSES)

    """
        :return predictions on the data given by input
    """

    def predict_by_batch(self, data, batch_size=32):
        predictions = None
        target_nodes = data[0]
        neighborhood_nodes = data[1]
        mask = tf.convert_to_tensor(data[2], dtype=tf.bool)
        neighborhood_sizes = data[3]

        iterations = target_nodes.shape[0] / batch_size
        if target_nodes.shape[0] % batch_size != 0:
            iterations += 1

        for ibatch in range(int(iterations)):
            batch_target_nodes = get_batch(target_nodes, ibatch, batch_size)
            batch_neighborhoods = get_batch(neighborhood_nodes, ibatch, batch_size)
            batch_mask = get_batch(mask, ibatch, batch_size)
            batch_neighborhoods_sizes = get_batch(neighborhood_sizes, ibatch, batch_size)

            current_pred = self.call([batch_target_nodes, batch_neighborhoods, batch_mask, batch_neighborhoods_sizes],
                                     training=False)
            predictions = current_pred if ibatch == 0 else tf.concat([predictions, current_pred], axis=0)
        return predictions
